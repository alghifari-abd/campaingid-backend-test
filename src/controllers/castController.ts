import { Request, Response } from "express";
import { Cast } from "../entities/Cast";
import { getRepository } from "typeorm";

export const getCasts = async (
    _:any,
    res: Response
): Promise<Response> => {
    const casts = await getRepository(Cast).find({relations: ['movie']});

    return res.json([casts]);
}

export const getCast = async (
    req: Request,
    res: Response
): Promise<Response> => {
    const cast = await getRepository(Cast).findOne(req.params.id, {relations : ['movie']});

    return res.json(cast)
}

export const createCast = async (
    req: Request,
    res: Response
): Promise<Response> => {
    const newCast = await getRepository(Cast).create(req.body);
    const theCast = await getRepository(Cast).save(newCast);

    return res.json(theCast);
}

export const updateCast = async (
    req: Request,
    res: Response
): Promise<Response> => {
    const cast = await getRepository(Cast).findOne(req.params.id)
    if(cast) {
        getRepository(Cast).merge(cast, req.body);
        const updatedCast = await getRepository(Cast).save(cast);

        return res.json(updatedCast)
    }

    return res.json({msg: 'No cast found'})
}

export const deleteCast = async (
    req: Request,
    res: Response
): Promise<Response> => {
    const deletedCast = await getRepository(Cast).delete(req.params.id)

    return res.json(deletedCast)
}
