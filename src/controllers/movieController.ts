import { Request, Response } from "express";
import { Movie } from "../entities/Movie";
import { getRepository } from "typeorm";

export const getMovies = async (
    _:any,
    res: Response
): Promise<Response> => {
    const movies = await getRepository(Movie).find({relations: ['casts']});

    return res.json([movies]);
}

export const getMovie = async (
    req: Request,
    res: Response
): Promise<Response> => {
    const movie = await getRepository(Movie).findOne(req.params.id, {relations: ['casts']});

    return res.json(movie)
}

export const createMovie = async (
    req: Request,
    res: Response
): Promise<Response> => {
    const newMovie = await getRepository(Movie).create(req.body);
    const theMovie = await getRepository(Movie).save(newMovie);

    return res.json(theMovie);
}

export const updateMovie = async (
    req: Request,
    res: Response
): Promise<Response> => {
    const movie = await getRepository(Movie).findOne(req.params.id)
    if(movie) {
        getRepository(Movie).merge(movie, req.body);
        const updatedMovie = await getRepository(Movie).save(movie);

        return res.json(updatedMovie)
    }

    return res.json({msg: 'No cast found'})
}

export const deleteMovie = async (
    req: Request,
    res: Response
): Promise<Response> => {
    const deletedMovie = await getRepository(Movie).delete(req.params.id)

    return res.json(deletedMovie)
}
