import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Movie } from "./Movie";

@Entity()
export class Cast {

    @PrimaryGeneratedColumn()
    id: bigint

    @Column({ type: "varchar", length: 100 })
    name: string

    @Column()
    birthday: Date

    @Column({nullable: true})
    deadday: Date

    @Column()
    rating: number

    @ManyToOne(() => Movie, movie => movie.casts)
    movie: Movie
}