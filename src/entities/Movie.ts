import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Cast } from "./Cast";

@Entity()
export class Movie {
    @PrimaryGeneratedColumn()
    id!: bigint

    @Column({ type: "varchar", length: 100 })
    name: string

    @Column({ type: "varchar", length: 30 })
    language: string

    @Column({ type: "varchar", length: 10 })
    status: string

    @Column()
    rating: number 
    
    @OneToMany(() => Cast, cast => cast.movie)
    casts: Cast
}