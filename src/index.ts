import "reflect-metadata";
import cors from "cors";
import express from "express";
import { createConnection } from "typeorm";
import morgan from "morgan";
import movieRoutes from "./routes/movieRoutes"
import castRoutes from "./routes/castRoutes"

const main = async () => {

  const app = express();
  createConnection();

  //app middleware
  app.use(cors());
  app.use(express.json());
  app.use(morgan("dev"))

  //app routes
  app.use(movieRoutes);
  app.use(castRoutes)

  app.listen(3000, () => {
    console.log("server started on localhost:3000");
  });
};

main().catch((err) => {
  console.error(err);
});
