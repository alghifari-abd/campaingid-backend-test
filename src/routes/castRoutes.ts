import { Router } from "express";
import { createCast, deleteCast, getCast, getCasts, updateCast } from "../controllers/castController";

const cast = Router();

cast.get("/casts", getCasts);
cast.get("/casts/:id", getCast);
cast.post("/casts", createCast);
cast.put("/casts/:id", updateCast);
cast.delete("/casts/:id", deleteCast);

export default cast