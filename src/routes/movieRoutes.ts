import { Router } from "express";
import { createMovie, deleteMovie, getMovie, getMovies, updateMovie } from "../controllers/movieController";

const movie = Router();

movie.get("/movies", getMovies);
movie.get("/movies/:id", getMovie);
movie.post("/movies", createMovie);
movie.put("/movies/:id", updateMovie);
movie.delete("/movies/:id", deleteMovie);

export default movie